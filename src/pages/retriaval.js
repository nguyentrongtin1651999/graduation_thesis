import { Modal, Button,Form} from "react-bootstrap";
import'../styles/retrieval.css'
import React, { Component } from 'react'
import ModalImage from "./modalImage";

const list = 
    {
        "status": "OK",
        "message": "Ahihi",
        "data": [
            {
                "extracted_text": "\nGuitar Cafe\nTel. 098 526 8359\n28 Phan Chu Trinh, Hiệp Phú, Quận 9, Hoa\nKhu vuc: Nhac\nNgày lập: 03/10/2020\nBàn: S02\nHóa đơn: HDL201003-0049\nPHIẾU TÍNH TIÊN\nSố lượng\nTiền\nĐongiá\nMón\nBia Tiger(lon) - Lon\n40,000\n2\n80,000\nThành tiên: 80,000\nCảm ơn quý khách, hẹn gắp lại í",
                "img_url": "https://graduationthesisleo.s3-ap-southeast-1.amazonaws.com/imgs/20201119_111837.jpg"
            },
            {
                "extracted_text": "\nGuitar Cafe\nTel. 098 526 8359\n28 Phan Chu Trinh, Hiệp Phú, Quận 9, Hoa\nKhu vuc: Nhac\nNgày lập: 03/10/2020\nBàn: S02\nHóa đơn: HDL201003-0049\nPHIẾU TÍNH TIÊN\nSố lượng\nTiền\nĐongiá\nMón\nBia Tiger(lon) - Lon\n40,000\n2\n80,000\nThành tiên: 80,000\nCảm ơn quý khách, hẹn gắp lại í",
                "img_url": "https://graduationthesisleo.s3-ap-southeast-1.amazonaws.com/imgs/20201119_111837.jpg"
            },
          
        ]
    };
  
export class RetrievalModal  extends Component {
    state = {
        isOpen: false,
        error: null,
        isLoaded: false,
        value:'',
        items: [],  
        hovered: false
      };
      handleChange = (event)=> {
        this.setState({value: event.target.value});
      }
      handlerSubmit = ()=>{
        fetch("http://18.138.227.214:13000/retrieve?text="+this.state.value)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                items: result.data
              });
            },
      
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }
      openModal = () => this.setState({ isOpen: true });
      closeModal = () => this.setState({ isOpen: false,items:[], isLoaded :false});
      render(){
        return (
            <>
             <Button  variant="primary" onClick={this.openModal} className="btn-button-retri">
              <span className="text-button-retri">
                  Retrieval
              </span>
              </Button>
              <Modal  show={this.state.isOpen} onHide={this.closeModal} className="scrollbar scrollbar-default ">
                <Modal.Body  className=" main-modal-retri"> 
               <div className="input-retri">
               <p className>SEARCH</p>
                <Form inline>
                <Form.Group controlId="exampleForm.ControlInput1" >
                    <Form.Control type="text" placeholder="Search" value={this.state.value} onChange={this.handleChange} />
                </Form.Group>
                <Button variant="primary"  onClick={this.handlerSubmit} >
                    Submit 
                </Button>
                </Form>
               </div>
               {this.state.isLoaded ? <div className="scrollbar scrollbar-default   result-retri" >
                   {
                       this.state.items.map((item)=><div className="item-retri ">
                            
                               {/* <img src={item.img_url} style={{ width: 280 ,height: 450 } }/> */}
                               <p className ="img-retri"><ModalImage
                                    src={item.img_url }
                                    ratio={`1:2`}
                                /></p>
                            <div className="scrollbar scrollbar-primary str-retri">
                            <p>{item.extracted_text.split('\n').map(str => <p>{str}</p>)}</p>
                            </div>
                       </div>
                       )
                       }
               </div>: <div>Loading...</div>}
                </Modal.Body>
              </Modal>
            </>
          );
       
        }
    }
            
  export default RetrievalModal