import { Modal, Button,ProgressBar } from "react-bootstrap";
import'../styles/modal.css'
import React, { Component } from 'react'
// import axios from 'axios'
  
export class AModal  extends Component {
  constructor(props) {
    super(props);
    this.state = {
    file: '',
    img: '',  
    isOpen: false,
    isOpen1: false,
    isOpen2: false,
    per : 0,
    per1 : 0,
    isLoading:false,
    items: "",
    images: "",
  };
  }
      openModal = () => this.setState({ isOpen: true });
      closeModal = () => this.setState({ isOpen: false});
      openModal1 = () => this.setState({ isOpen1: true ,});
      closeModal1 = () => this.setState({ isOpen1: false, isOpen2: true});
      openModal2 = () => this.setState({ isOpen2: true });
      closeModal2 = () => this.setState({ isOpen2: false ,items :"", images : "",isLoading:false});
      _handleSubmit(e) {
        e.preventDefault();
        // TODO: do something with -> this.state.file
        console.log('handle uploading-', this.state.file);
      }
      _handleImageChange(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
    
        reader.onloadend = () => {
          this.setState({
            file: file,
            img: reader.result
          });   
        }   
        reader.readAsDataURL(file)
        this.setState({ isOpen: true ,isOpen2:false,isOpen1 : false,})
      }
      submitHandler=()=>{
      var formdata = new FormData();
      formdata.append("img", this.state.file);
      formdata.append("language", "vie");
      var requestOptions = {
        method: 'POST',
        body: formdata,
        redirect: 'follow'
      };
      this.setState({ isOpen: false ,isOpen2:true,isOpen1 : false,})
        fetch("http://18.138.227.214:13000/kie", requestOptions)
        .then(response => response.json())
        .then(result => {
          console.log(result)
          this.setState({
            items : result.data["extracted_text"],
            images: result.data["img_url"],
            isLoading: true
          })
        }).catch(error => console.log('error', error));
      }
      render(){
        let {img} = this.state;
        let $img = null;
        if (img) {
          $img = (<img src={img} className ="imgpre"/>);
        } else {
          $img = (<div className="previewText">Please select an Image for Preview</div>);
        }
        return (
            <>
             <Button  variant="primary" onClick={this.openModal} className="btn-button">
              <span className="text-button">
                  +
              </span>
              </Button>
              <p className="text-title-button">Put your data</p> 
              <Modal  show={this.state.isOpen} onHide={this.closeModal}>
                <Modal.Body  className="main-modal"> 
                  <form onSubmit={(e)=>this._handleSubmit(e)}>
                   <input  type="file"  onChange={(e)=>this._handleImageChange(e)} className="btn-inmodal" />
                 </form> 
                 <button  onClick={this.submitHandler} className="btn-md2">
                               Next
                      </button>  
                </Modal.Body>
              </Modal>

              {/* <Modal  show={this.state.isOpen1} onHide={this.closeModal1}>
                <Modal.Body  className="main-modal"> 
                        <div className="load-1">
                          <div className="avt">
                          </div>
                          <div className = "progress-1">
                            <p>{this.state.per}%</p>
                          <ProgressBar animated now={this.state.per} />
                          {this.state.isDone?<p>Done</p>:<p> Processing</p>}
                          </div>
                        </div>
                        <div className="load-2">
                        <div className="avt"></div>
                        <div className = "progress-1">
                            <p>{this.state.per1}%</p>
                          <ProgressBar animated now={this.state.per1} />
                          {this.state.isDone1?<p>Done</p>:<p> Processing</p>}
                          </div>
                        </div> 
                       <button  onClick={this.submitHandler} className="btn-md2">
                               Next
                      </button>  
                    
                </Modal.Body>
              </Modal> */}
              <Modal  show={this.state.isOpen2} onHide={this.closeModal2}>    
              {this.state.isLoading == true ?
              <Modal.Body  className="main-modal"> 
                        <div className="content-1">
                           <img src={this.state.images} style={{ width: 280 ,height: 450 }}/>
                        </div>
                        <div className="scrollbar scrollbar-primary content-2">
                        {this.state.items.split('\n').map(str => <p>{str}</p>)}
                        </div>
                        <Button onClick={this.closeModal2}>
                                Close
                        </Button>   
                </Modal.Body>:
                <Modal.Body  className="main-modal">
                  <div>Loading...</div>
                 </Modal.Body>
                }
                
              </Modal> 
            </>
          );
       
        }
    }
            
  export default AModal