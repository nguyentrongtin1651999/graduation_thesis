import React, { Component } from 'react'
import '../styles/home_page.css'
import  AModal  from './modal';
import RetrievalModal from './retriaval';
import'../styles/retrieval.css'

export class HomePage  extends Component {
  render() {
    return  <div className="background">
    <div className="background-circle background-circle-1">
    </div>
    <div className="glassmorphism1">
      <div  className="glassmorphism2">
         <div className="main-menu">
            <p>Invoice</p>
            <p>information</p>
            <p>Extraction</p>
        </div>
        <div className="button-modal-retrieval">
          <RetrievalModal/>
        </div>
        <div className="button-modal">
          <AModal/>
        </div>
      </div>
      <div className="sub-content" >
        <div className="sub-content1">
         </div>
        <div className="sub-content2"> </div>
      </div>

    </div>
    
   </div>
  }
}

export default HomePage